var gulp = require('gulp'),
    concat = require('gulp-concat'),
    rename = require("gulp-rename"),
    open = require('gulp-open'),
    del = require('del'),
    uglify = require('gulp-uglify'),
    cleanCSS = require('gulp-clean-css'),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant');

gulp.task('clean', function() {
    return del('build');
});

gulp.task('images', function() {
    return gulp.src('src/img/**/*.png')
        .pipe(imagemin({
            use: [pngquant()]
        }))
        .pipe(gulp.dest('build/img'));
});

gulp.task('css:minify', function() {
    return gulp.src('src/css/**/*.css')
        .pipe(concat('lnu-map.css'))
        .pipe(cleanCSS())
        .pipe(rename('lnu-map.min.css'))
        .pipe(gulp.dest('build/css'));
});

gulp.task('js:minify', function(callback) {
    gulp.src('src/js/map.js')
        .pipe(uglify())
        .pipe(rename('lnu-map.min.js'))
        .pipe(gulp.dest('build/js'));

    gulp.src(['src/js/**/*.js', '!src/js/map.js'])
        .pipe(concat('lnu-data.js'))
        .pipe(uglify())
        .pipe(rename('lnu-map.data.min.js'))
        .pipe(gulp.dest('build/js'));

    callback();
});

gulp.task('html', function() {
    return gulp.src('src/assets/index.build.html')
        .pipe(rename('index.html'))
        .pipe(gulp.dest('build/'));
});

gulp.task('libs', function() {
    return gulp.src(['src/libs/**/*.min.*{js,css}', 'src/libs/**/*.png'])
        .pipe(gulp.dest('build/libs'));
});

gulp.task('build', gulp.series(
    'clean',
    gulp.parallel('css:minify', 'js:minify', 'images', 'html', 'libs'))
);

gulp.task('run:debug', function(callback) {
    gulp.src('src/index.html')
        .pipe(open());

    callback();
});

gulp.task('run:release', function(callback) {
    gulp.src('build/index.html')
        .pipe(open());

    callback();
});

gulp.task('default', gulp.series('build', 'run:release'));