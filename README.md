*Interactive map, as voluntary project for [LNU University](http://www.lnu.edu.ua/).
Helps you to find all university buildings, best routes and your location.*

## Map is hosted on the [university page](http://www.lnu.edu.ua/about/contact-information/interactive-map/) ##

## UI Overview
![map.png](https://bitbucket.org/repo/Rq6Lj4/images/4093903220-map.png)

## Build
- run 'nmp install' to install all dependecies
- run 'gulp build' to build project

---

## Usage
```html
<head>
    <link rel="stylesheet" href="libs/chosen/chosen.min.css" /> <!-- Autocomplete styles -->
    <link rel="stylesheet" href="css/lnu-map.min.css" />		<!-- Main map styles -->
</head>

<body>

    <div id="lnu-map"></div>									<!-- Add only this div to your page -->

    <!-- Include jquery and google maps api here -->
	
    <script src="libs/gmaps/gmaps.min.js" type="text/javascript"></script>				<!-- Nice google map wrapper -->
    <script src="libs/chosen/chosen.jquery.min.js" type="text/javascript"></script>		<!-- Choosen autocomplete -->
    <script src="js/lnu-map.data.min.js"></script>										<!-- Map data -->
    <script src="js/lnu-map.min.js"></script>											<!-- Interactive lnu-map library -->
</body>
```

---

## Dependicies
* jQuery
* 	[chosen](https://harvesthq.github.io/chosen/)
* 	[gmaps.js](https://hpneo.github.io/gmaps/)	

### Map icons
* [https://mapicons.mapsmarker.com/](https://mapicons.mapsmarker.com/)

---    

**Build with with Visual Studio Code**