/* This can be easily replaced with json and featched via ajax - but is this needed ?
  Also polygon data can be moved to xml and featched as KmlLayer.
  For now - this is flexible enough and has reasonably small size. 
*/

/** 
 * @param markers Main data object.
 * @param {Object} faculties - Marker type group(same as others).
 * @param {String} faculties.markerIconUrl - Default group marker.
 * @param {Object[]} faculties.markerData - Markers data array in group.
 * @param {String|HTMLElement} faculties.markerData.name - Marker title, can be a html content.
 * @param {Number} residences.markerData.lat - Marker latitude.
 * @param {Number} residences.markerData.lng - Marker longitude.
 * @param {String} [residences.markerData.customIconUrl] - Will override defauld group icon.
 * @param {String} [residences.markerData.href] - From title with href and name.
 * @param {String} [residences.markerData.content] - Additional information shown in info window.
*/
var markers = {
    faculties: {
        title: "Факультети та коледжі",
        markerIconUrl: "img/faculty.png",
        markerData: [
            {
                id: "faculty-history",
                name: "Історичний факультет",
                lat: 49.840463,
                lng: 24.022926,
                href: 'http://clio.lnu.edu.ua/'
            },
            {
                id: "faculty-languages",
                name: "Факультет іноземних мов",
                lat: 49.840632,
                lng: 24.022701,
                href: 'http://lingua.lnu.edu.ua'
            },
            {
                id: "faculty-mexmat",
                name: "Механіко-математичний факультет",
                lat: 49.840899,
                lng: 24.022208,
                href: 'http://www.mmf.lnu.edu.ua'
            },
            {
                id: "faculty-appliedmath",
                name: "Факультет прикладної математики та інформатики",
                lat: 49.840764,
                lng: 24.021929,
                href: 'http://ami.lnu.edu.ua'
            },
            {
                id: "faculty-filology",
                name: "Філологічний факультет",
                lat: 49.840296,
                lng: 24.023146,
                href: 'http://philology.lnu.edu.ua'
            },
            {
                id: "faculty-philosophy",
                name: "Філософський факультет",
                lat: 49.839878,
                lng: 24.023226,
                href: 'http://filos.lnu.edu.ua'
            },
            {
                id: "faculty-culture",
                name: "Факультет культури і мистецтв",
                lat: 49.839872,
                lng: 24.034738,
                href: 'http://kultart.lnu.edu.ua'
            },
            {
                id: "faculty-electronics",
                name: "Факультет електроніки",
                lat: 49.832189,
                lng: 24.027408,
                href: 'http://electronics.lnu.edu.ua'
            },
            {
                id: "faculty-electronics2",
                name: "Факультет електроніки",
                lat: 49.827565,
                lng: 24.041487,
                href: 'http://electronics.lnu.edu.ua'
            },
            {
                id: "faculty-geography",
                name: "Географічний факультет",
                lat: 49.837275,
                lng: 24.023360,
                href: 'http://geography.lnu.edu.ua'
            },
            {
                id: "faculty-biology",
                name: "Біологічний факультет",
                lat: 49.834142,
                lng: 24.031965,
                href: 'http://bioweb.lnu.edu.ua'
            },
            {
                id: "faculty-geology",
                name: "Геологічний факультет",
                lat: 49.833990,
                lng: 24.032241,
                href: 'http://geology.lnu.edu.ua'
            },
            {
                id: "faculty-law",
                name: "Юридичний факультет",
                lat: 49.840000,
                lng: 24.023430,
                href: 'http://law.lnu.edu.ua'
            },
            {
                id: "faculty-relat",
                name: "Факультет міжнародних відносин",
                lat: 49.839338,
                lng: 24.023457,
                href: 'http://intrel.lnu.edu.ua'
            },
            {
                id: "faculty-chemistry",
                name: "Хімічний факультет",
                lat: 49.832300,
                lng: 24.030328,
                href: 'http://chem.lnu.edu.ua'
            },
            {
                id: "faculty-physics",
                name: "Фізичний факультет",
                lat: 49.831858,
                lng: 24.030140,
                href: 'http://physics.lnu.edu.ua'
            },
            {
                id: "faculty-econom",
                name: "Економічний факультет",
                lat: 49.842586,
                lng: 24.028338,
                href: 'http://econom.lnu.edu.ua'
            },
            {
                id: "faculty-journal",
                name: "Факультет журналістики",
                lat: 49.830438,
                lng: 24.008650,
                href: 'http://journ.lnu.edu.ua'
            },
            {
                id: "faculty-pedagog",
                name: "Факультет педагогічної освіти",
                lat: 49.836800,
                lng: 24.039539,
                href: 'http://pedagogy.lnu.edu.ua'
            },
            {
                id: "faculty-pedagog2",
                name: "Факультет педагогічної освіти",
                lat: 49.834094,
                lng: 24.005389,
                href: 'http://pedagogy.lnu.edu.ua'
            },
            {
                id: "faculty-bussines",
                name: "Факультет правління фінансами та бізнесу",
                lat: 49.839045,
                lng: 24.028061,
                href: 'http://financial.lnu.edu.ua'
            },
            {
                id: "faculty-colped",
                name: "Педагогічний коледж",
                lat: 49.836700,
                lng: 24.039752,
                customIconUrl: "img/college.png",
                href: 'http://pedcollege.lnu.edu.ua'
            },
            {
                id: "faculty-collaw",
                name: "Правничий коледж",
                lat: 49.838092,
                lng: 24.025012,
                customIconUrl: "img/college.png",
                href: 'http://lawcollege.lnu.edu.ua'
            },
            {
                id: "faculty-colnature",
                name: "Природничий коледж",
                lat: 49.827529,
                lng: 24.041556,
                customIconUrl: "img/college.png",
                href: 'http://natcollege.lnu.edu.ua'
            },
            {
                id: "faculty-aftergrad",
                name: "Інститут післядипломної освіти",
                lat: 49.839753,
                lng: 24.023025,
                customIconUrl: "img/postcollege.png",
                href: 'http://ipodp.lnu.edu.ua/'
            },
        ]
    },
    residences: {
        title: "Гуртожитки",
        markerIconUrl: "img/residence.png",
        markerData: [
            {
                id: "residence-1",
                name: "Гуртожиток №1",
                lat: 49.835564,
                lng: 24.033667,
                href: 'http://campus.lnu.edu.ua/gurtozhytky/gurtozhytok-1/'
            },
            {
                id: "residence-2",
                name: "Гуртожиток №2",
                lat: 49.825354,
                lng: 24.079525,
                href: 'http://campus.lnu.edu.ua/gurtozhytky/gurtozhytok-2/'
            },
            {
                id: "residence-3",
                name: "Гуртожиток №3",
                lat: 49.825629,
                lng: 24.078217,
                href: 'http://campus.lnu.edu.ua/gurtozhytky/gurtozhytok-3/'
            },
            {
                id: "residence-4",
                name: "Гуртожиток №4",
                lat: 49.831318,
                lng: 24.065995,
                href: 'http://campus.lnu.edu.ua/gurtozhytky/gurtozhytok-4/'
            },
            {
                id: "residence-5",
                name: "Гуртожиток №5",
                lat: 49.828409,
                lng: 24.068720,
                href: 'http://campus.lnu.edu.ua/gurtozhytky/gurtozhytok-5/'
            },
            {
                id: "residence-6",
                name: "Гуртожиток №6",
                lat: 49.827890,
                lng: 24.068715,
                href: 'http://campus.lnu.edu.ua/gurtozhytky/gurtozhytok-6/'
            },
            {
                id: "residence-7",
                name: "Гуртожиток №7",
                lat: 49.864192,
                lng: 24.008999,
                href: 'http://campus.lnu.edu.ua/gurtozhytky/gurtozhytok-7/'
            },
            {
                id: "residence-8",
                name: "Гуртожиток №8",
                lat: 49.828180,
                lng: 24.069747
            }
        ]
    },
    canteens: {
        title: "Їдальні та буфети",
        markerIconUrl: "img/buffet.png",
        markerData: [
            {
                id: "canteen-main",
                name: "Їдальня головного корпусу",
                lat: 49.839976,
                lng: 24.023338,
                customIconUrl: "img/canteen.png"
            },
            {
                id: "canteen-economy",
                name: "Їдальня економічного факультету",
                lat: 49.842440,
                lng: 24.028304,
                customIconUrl: "img/canteen.png"
            },
            {
                id: "canteen-nature",
                name: "Їдальня Природничого коледжу",
                lat: 49.827697,
                lng: 24.041181,
                customIconUrl: "img/canteen.png"
            },
            {
                id: "canteen-1",
                name: "Буфет № 1",
                lat: 49.839987,
                lng: 24.023226
            },
            {
                id: "canteen-2",
                name: "Буфет № 2",
                lat: 49.837462,
                lng: 24.023092
            },
            {
                id: "canteen-3",
                name: "Буфет № 3",
                lat: 49.836907,
                lng: 24.039308
            },
            {
                id: "canteen-4",
                name: "Буфет № 4",
                lat: 49.833903,
                lng: 24.031884
            },
            {
                id: "canteen-5",
                name: "Буфет № 5",
                lat: 49.832317,
                lng: 24.030690
            },
            {
                id: "canteen-6",
                name: "Буфет № 6",
                lat: 49.832294,
                lng: 24.061703
            },
            {
                id: "canteen-8",
                name: "Буфет № 8",
                lat: 49.839776,
                lng: 24.034504
            },
            {
                id: "canteen-12",
                name: "Буфет № 12",
                lat: 49.831961,
                lng: 24.027365
            },
            {
                id: "canteen-journal",
                name: "Буфет факультету журналістики",
                lat: 49.830376,
                lng: 24.008744
            }
        ]
    },
    parkings: {
        title: "Парковки",
        markerIconUrl: "img/parking-bicycle.png",
        markerData: [
            {
                id: "parking-1",
                name: "Велопарковка",
                lat: 49.839829,
                lng: 24.022094
            },
            {
                id: "parking-2",
                name: "Велопарковка",
                lat: 49.836929,
                lng: 24.023145
            },
            {
                id: "parking-3",
                name: "Велопарковка",
                lat: 49.842641,
                lng: 24.027748
            },
            {
                id: "parking-4",
                name: "Велопарковка",
                lat: 49.839689,
                lng: 24.035317
            },
            {
                id: "parking-5",
                name: "Велопарковка",
                lat: 49.836624,
                lng: 24.039957
            },
            {
                id: "parking-6",
                name: "Велопарковка",
                lat: 49.834485,
                lng: 24.031783
            },
            {
                id: "parking-7",
                name: "Велопарковка",
                lat: 49.832207,
                lng: 24.029942
            },
            {
                id: "parking-8",
                name: "Велопарковка",
                lat: 49.832064,
                lng: 24.027590
            },
            {
                id: "parking-9",
                name: "Велопарковка",
                lat: 49.827676,
                lng: 24.041519
            },
            {
                id: "parking-10",
                name: "Велопарковка",
                lat: 49.830410,
                lng: 24.008425
            },
            {
                id: "parking-11",
                name: "Велопарковка",
                lat: 49.834198,
                lng: 24.005309
            },
            {
                id: "parking-12",
                name: "Парковка",
                lat: 49.841075,
                lng: 24.023017,
                customIconUrl: "img/parking.png",
            },
            {
                id: "parking-13",
                name: "Парковка",
                lat: 49.839131,
                lng: 24.027916,
                customIconUrl: "img/parking.png",
            },
            {
                id: "parking-14",
                name: "Платна парковка",
                lat: 49.841511,
                lng: 24.025442,
                customIconUrl: "img/parking.png",
            },
            {
                id: "parking-15",
                name: "Платна парковка",
                lat: 49.839727,
                lng: 24.030024,
                customIconUrl: "img/parking.png",
            },
            {
                id: "parking-16",
                name: "Платна парковка",
                lat: 49.839256,
                lng: 24.031172,
                customIconUrl: "img/parking.png",
            },
            {
                id: "parking-17",
                name: "Велогараж(3ій гуртожиток)",
                lat: 49.825612,
                lng: 24.078120
            },
            {
                id: "parking-18",
                name: "Велогараж(6ий гуртожиток)",
                lat: 49.827868,
                lng: 24.068345
            }
        ]
    },
    pcrooms: {
        title: "Комп\'ютерні класи",
        markerIconUrl: "img/computers.png",
        markerData: [
            {
                id: "pcroom-main",
                name: "Комп’ютерний клас (головний корпус)",
                lat: 49.840689,
                lng: 24.022211
            },
            {
                id: "pcroom-law",
                name: "Комп’ютерний клас (юридичний факультет)",
                lat: 49.840075,
                lng: 24.023368
            },
            {
                id: "pcroom-geography",
                name: "Комп’ютерний клас (географічний факультет)",
                lat: 49.836910,
                lng: 24.023689
            },
            {
                id: "pcroom-economy",
                name: "Комп’ютерний клас (економічний факультет)",
                lat: 49.842440,
                lng: 24.028485
            },
            {
                id: "pcroom-cultart",
                name: "Комп’ютерний клас (факультет культури і мистецтв)",
                lat: 49.839725,
                lng: 24.035022
            },
            {
                id: "pcroom-biology",
                name: "Комп’ютерний клас (біологічний факультет)",
                lat: 49.833941,
                lng: 24.031687
            },
            {
                id: "pcroom-researchcenter",
                name: "Комп’ютерний клас (Центр гуманітарних досліджень)",
                lat: 49.835569,
                lng: 24.032837
            },
            {
                id: "pcroom-electronics2",
                name: "Комп’ютерний клас (факультет електроніки)",
                lat: 49.832141,
                lng: 24.027295
            },
            {
                id: "pcroom-chemistry",
                name: "Комп’ютерний клас (хімічний фікультет)",
                lat: 49.832447,
                lng: 24.030021
            },
            {
                id: "pcroom-physics",
                name: "Комп’ютерний клас (фізичний факультет)",
                lat: 49.831808,
                lng: 24.030032
            },
            {
                id: "pcroom-journal",
                name: "Комп’ютерний клас (факультет журналістики)",
                lat: 49.830479,
                lng: 24.008793
            },
            {
                id: "pcroom-electronics",
                name: "Комп’ютерний клас (факультет електроніки)",
                lat: 49.827448,
                lng: 24.041725
            }
        ]
    },
    libraries: {
        title: "Бібліотеки",
        markerIconUrl: "img/book.png",
        markerData: [
            {
                id: "library-main",
                name: "Наукова бібліотека",
                lat: 49.834089,
                lng: 24.030623,
                href: 'http://library.lnu.edu.ua/bibl/'
            },
            {
                id: "library-main2",
                name: "Наукова бібліотека",
                lat: 49.833233,
                lng: 24.029510,
                href: 'http://library.lnu.edu.ua/bibl/'
            },
            {
                id: "library-biology",
                name: "Бібліотека біологічного факультету",
                lat: 49.835548,
                lng: 24.032777
            },
            {
                id: "library-geography",
                name: "Бібліотека географічного факультету",
                lat: 49.837147,
                lng: 24.023537
            },
            {
                id: "library-geology",
                name: "Бібліотека геологічного факультету",
                lat: 49.833983,
                lng: 24.031981
            },
            {
                id: "library-economy",
                name: "Бібліотека економічного факультету",
                lat: 49.842551,
                lng: 24.028205
            },
            {
                id: "library-journal",
                name: "Бібліотека факультету журналістики",
                lat: 49.830361,
                lng: 24.008613
            },
            {
                id: "library-relat",
                name: "Бібліотека факультету міжнародних відносин",
                lat: 49.839871,
                lng: 24.023124
            },
            {
                id: "library-math",
                name: "Бібліотека математичних факультетів (факультети: механіко-математичний, прикладної математики та інформатики)",
                lat: 49.840746,
                lng: 24.022034
            },
            {
                id: "library-math2",
                name: "Кабінет наукової математичної літератури",
                lat: 49.840612,
                lng: 24.022536
            },
            {
                id: "library-art",
                name: "Кабінет мистецтвознавства",
                lat: 49.840389,
                lng: 24.022809
            },
            {
                id: "library-filosophy",
                name: "Кабінет філософії",
                lat: 49.840173,
                lng: 24.023081
            },
            {
                id: "library-electronics",
                name: "Бібліотека фізичного факультету (факультети електроніки)",
                lat: 49.834635,
                lng: 24.030845
            },
            {
                id: "library-chemistry",
                name: "Бібліотека хімічного факультету",
                lat: 49.831884,
                lng: 24.030161
            },
            {
                id: "library-law",
                name: "Бібліотека юридичного факультету (правничий коледж)",
                lat: 49.838037,
                lng: 24.025045
            },
            {
                id: "library-pedagog",
                name: "Бібліотека педагогічного коледжу",
                lat: 49.836866,
                lng: 24.039362
            },
            {
                id: "library-pedagog2",
                name: "Бібліотека кафедри педагогіки",
                lat: 49.836728,
                lng: 24.039662
            },
            {
                id: "library-astronomy",
                name: "Бібліотека астрономічної обсерваторії",
                lat: 49.831770,
                lng: 24.030027
            },
            {
                id: "library-stud2",
                name: "Бібліотека студентського гуртожитоку № 2",
                lat: 49.825610,
                lng: 24.079600
            },
            {
                id: "library-stud3",
                name: "Бібліотека студентського гуртожитоку № 3",
                lat: 49.825857,
                lng: 24.078474
            },
            {
                id: "library-stud4",
                name: "Бібліотека студентського гуртожитоку № 4",
                lat: 49.831138,
                lng: 24.066210
            },
            {
                id: "library-naturefacs",
                name: "Бібліотека гуманітарних факультетів (факультети: історичний, філологічний, культури та мистецтв, філософський, іноземних мов)",
                lat: 49.837950,
                lng: 24.022166
            }
        ]
    },
    museums: {
        title: "Музеї",
        markerIconUrl: "img/museums.png",
        markerData: [
            {
                id: "museum-zoology",
                name: "Зоологічний музей",
                lat: 49.834345,
                lng: 24.030970,
                href: 'http://museums.lnu.edu.ua/zoology/'
            },
            {
                id: "museum-ruda",
                name: "Музей рудних формацій",
                lat: 49.834281,
                lng: 24.031101,
                href: 'http://museums.lnu.edu.ua/ore-formations/'
            },
            {
                id: "museum-mineral",
                name: "Мінералогічний музей",
                lat: 49.834197,
                lng: 24.031241,
                href: 'http://museums.lnu.edu.ua/mineralogy/'
            },
            {
                id: "museum-paleontology",
                name: "Палеонтологічний музей",
                lat: 49.834115,
                lng: 24.031394,
                href: 'http://museums.lnu.edu.ua/paleontology/'
            },
            {
                id: "museum-archeology",
                name: "Археологічний музей",
                lat: 49.840151,
                lng: 24.022971,
                href: 'http://museums.lnu.edu.ua/archeology/'
            },
            {
                id: "museum-lnuhistory",
                name: "Музей історії Університету",
                lat: 49.840575,
                lng: 24.022350,
                href: 'http://museums.lnu.edu.ua/history/'
            },
            {
                id: "museum-flowers",
                name: "Гербарій",
                lat: 49.834283,
                lng: 24.030992,
                customIconUrl: "img/flowers.png",
                href: 'http://herbarium.lnu.edu.ua/natsionalne-nadbannya-ukrajiny/'
            },
            {
                id: "museum-viruses",
                name: "Колекція культур мікроорганізмів-продуцентів біологічно активних речовин ",
                lat: 49.834207,
                lng: 24.031133,
                customIconUrl: "img/virus.png",
                href: 'http://lv-microbcollect.lviv.ua/'
            },
            {
                id: "museum-zoologyfond",
                name: "Наукові фонди та музейна експозиція Зоологічного музею",
                lat: 49.834124,
                lng: 24.031282,
                customIconUrl: "img/mosquito.png",
                href: 'http://museums.lnu.edu.ua/zoology/'
            },
            {
                id: "museum-tropicflowers",
                name: "Колекція тропічних і субтропічних рослин Ботанічного саду загальнодержавного значення",
                lat: 49.830003,
                lng: 24.064918,
                customIconUrl: "img/flowers.png"
            },
            {
                id: "museum-tropicflowers2",
                name: "Колекція тропічних і субтропічних рослин Ботанічного саду загальнодержавного значення",
                lat: 49.832703,
                lng: 24.031262,
                customIconUrl: "img/flowers.png"
            },
            {
                id: "museum-books",
                name: "Фонд рукописних, стродуркованих та рідкісних книг Наукової бібліотеки",
                lat: 49.833211,
                lng: 24.029561,
                customIconUrl: "img/archive.png",
                href: 'http://library.lnu.edu.ua/bibl/index.php?option=com_content&view=article&id=62:2008-10-14-08-18-15&catid=42:2008-10-14-08-06-41&Itemid=54'
            },
            {
                id: "museum-astronomy",
                name: "Науково-дослідний комплекс апаратури для ввчення штучних небесних тіл ближного космосу Астрономічної обсерваторії",
                lat: 49.917532,
                lng: 23.954161,
                customIconUrl: "img/telescope.png",
                href: 'http://astro.lnu.edu.ua/natsionalne-nadbannya-ukrajiny/'
            },
            {
                id: "museum-garden",
                name: "Ботанічний сад",
                lat: 49.832629,
                lng: 24.031149,
                customIconUrl: "img/tree.png",
                href: 'http://botanicgarden.lnu.edu.ua/'
            },
            {
                id: "museum-garden2",
                name: "Ботанічний сад",
                lat: 49.830046,
                lng: 24.064961,
                customIconUrl: "img/tree.png",
                href: 'http://botanicgarden.lnu.edu.ua/'
            }
        ]
    },
    archive: {
        title: "Архів",
        markerIconUrl: "img/archive.png",
        markerData: [
            {
                id: "archive",
                name: 'Архів',
                lat: 49.837247,
                lng: 24.023341,
                href: 'http://archive.lnu.edu.ua/'
            }
        ]
    },
    clinics: {
        title: "Медпункти",
        markerIconUrl: "img/hospital.png",
        markerData: [
            {
                id: "clinic-hospital10",
                name: "Десята міська лікарня",
                lat: 49.828133,
                lng: 24.015435,
                href: 'http://www.lnu.edu.ua/about/subdivisions/sector-of-leisuorganising-and-medical-services/'
            },
            {
                id: "clinic-clinic",
                name: "Медичний пункт",
                lat: 49.840293,
                lng: 24.022684,
                href: 'http://www.lnu.edu.ua/about/subdivisions/sector-of-leisuorganising-and-medical-services/'
            }
        ]
    },
    stuff: {
        title: "Дозвілля",
        markerIconUrl: "img/freetime.png",
        markerData: [
            {
                id: "other-stadium",
                name: "Стадіон",
                lat: 49.832045,
                lng: 24.064128,
                customIconUrl: "img/running.png",
                href: 'http://www.lnu.edu.ua/academics/leisure/sports-groups-swimming-pool/'
            },
            {
                id: "other-sportcenter",
                name: "Спортивний комплекс",
                lat: 49.832107,
                lng: 24.062250,
                customIconUrl: "img/gym.png",
                href: 'http://www.lnu.edu.ua/academics/leisure/sports-groups-swimming-pool/'
            },
            {
                id: "other-pool",
                name: "Басейн",
                lat: 49.832308,
                lng: 24.061628,
                customIconUrl: "img/swimming.png",
                href: 'http://www.lnu.edu.ua/academics/leisure/sports-groups-swimming-pool/'
            },
            {
                id: "other-observatory",
                name: "Астрономічна обсерваторія",
                lat: 49.917556,
                lng: 23.954172,
                customIconUrl: "img/obs.png",
                href: 'http://astro.lnu.edu.ua/'
            },
            {
                id: "other-hall",
                name: "Дзеркальна та Актова зали",
                lat: 49.839999,
                lng: 24.022843,
                customIconUrl: "img/assemblyhall.png"
            },
            {
                id: "other-garden",
                name: "Ботанічний сад",
                lat: 49.832629,
                lng: 24.031149,
                customIconUrl: "img/tree.png",
                href: 'http://botanicgarden.lnu.edu.ua/'
            },
            {
                id: "other-garden2",
                name: "Ботанічний сад",
                lat: 49.830046,
                lng: 24.064961,
                customIconUrl: "img/tree.png",
                href: 'http://botanicgarden.lnu.edu.ua/'
            },
            {
                id: "other-freetime",
                name: "Центр культури та дозвілля",
                lat: 49.840497,
                lng: 24.022150,
                customIconUrl: "img/information.png",
                href: 'http://centres.lnu.edu.ua/culture-and-leisure/'
            }
        ]
    },
    departments: {
        title: "Кафедри",
        showInMenu: false,
        markerIconUrl: "img/department.png",
        markerData: [
            {
                id: "department-1",
                name: "Кафедра німецької філології",
                lat: 49.840168,
                lng: 24.022553
            },
            {
                id: "department-2",
                name: "Кафедра англійської філології ",
                lat: 49.840168,
                lng: 24.022553
            },
            {
                id: "department-3",
                name: "Кафедра світової літератури",
                lat: 49.840168,
                lng: 24.022553
            },
            {
                id: "department-4",
                name: "Кафедра французької філології",
                lat: 49.840168,
                lng: 24.022553
            },
            {
                id: "department-5",
                name: "Кафедра класичної філології",
                lat: 49.840168,
                lng: 24.022553
            },
            {
                id: "department-6",
                name: "Кафедра міжкультурної комунікації та перекладу",
                lat: 49.840168,
                lng: 24.022553
            },
            {
                id: "department-7",
                name: "Кафедра перекладознавства та контрастивної лінгвістики ім. Григорія Кочури",
                lat: 49.840168,
                lng: 24.022553
            },
            {
                id: "department-8",
                name: "Кафедра іноземних мов для природничих факультетів",
                lat: 49.840168,
                lng: 24.022553
            },
            {
                id: "department-9",
                name: "Кафедра іноземних мов для гуманітарних факультетів",
                lat: 49.840168,
                lng: 24.022553
            },
            {
                id: "department-10",
                name: "Кафедра історії Центральної та Східної Європи",
                lat: 49.840168,
                lng: 24.022553
            },
            {
                id: "department-11",
                name: "Кафедра нової і новітньої історії зарубіжних країн",
                lat: 49.840168,
                lng: 24.022553
            },
            {
                id: "department-12",
                name: "Кафедра новітньої історії України",
                lat: 49.840168,
                lng: 24.022553
            },
            {
                id: "department-13",
                name: "Кафедра історичного краєзнавства",
                lat: 49.840168,
                lng: 24.022553
            },
            {
                id: "department-14",
                name: "Кафедра археології та спеціальних галузей історичної науки",
                lat: 49.840168,
                lng: 24.022553
            },
            {
                id: "department-15",
                name: "Кафедра давньої історії України та архівознавства",
                lat: 49.840168,
                lng: 24.022553
            },
            {
                id: "department-16",
                name: "Кафедра історії та теорії соціології",
                lat: 49.840168,
                lng: 24.022553
            },
            {
                id: "department-17",
                name: "Кафедра етнології",
                lat: 49.840168,
                lng: 24.022553
            },
            {
                id: "department-18",
                name: "Кафедра історії середніх віків та візантиністики",
                lat: 49.840168,
                lng: 24.022553
            },
            {
                id: "department-19",
                name: "Кафедра диференціальних рівнянь",
                lat: 49.840168,
                lng: 24.022553
            },
            {
                id: "department-20",
                name: "Кафедра алгебри і логіки",
                lat: 49.840168,
                lng: 24.022553
            },
            {
                id: "department-21",
                name: "Кафедра геометрії і топології",
                lat: 49.840168,
                lng: 24.022553
            },
            {
                id: "department-22",
                name: "Кафедра вищої математики",
                lat: 49.840168,
                lng: 24.022553
            },
            {
                id: "department-23",
                name: "Кафедра теорії функцій і теорії ймовірностей",
                lat: 49.840168,
                lng: 24.022553
            },
            {
                id: "department-24",
                name: "Кафедра математичного і функціонального аналізу",
                lat: 49.840168,
                lng: 24.022553
            },
            {
                id: "department-25",
                name: "Кафедра теоретичної та прикладної статистики",
                lat: 49.840168,
                lng: 24.022553
            },
            {
                id: "department-26",
                name: "Кафедра механіки",
                lat: 49.840168,
                lng: 24.022553
            },
            {
                id: "department-27",
                name: "Кафедра математичної економіки та економетрії",
                lat: 49.840168,
                lng: 24.022553
            },
            {
                id: "department-28",
                name: "Кафедра математичного моделювання",
                lat: 49.840168,
                lng: 24.022553
            },
            {
                id: "department-29",
                name: "Кафедра прикладної математики",
                lat: 49.840168,
                lng: 24.022553
            },
            {
                id: "department-30",
                name: "Кафедра обчислювальної математики",
                lat: 49.840168,
                lng: 24.022553
            },
            {
                id: "department-31",
                name: "Кафедра теорії оптимальних процесів",
                lat: 49.840168,
                lng: 24.022553
            },
            {
                id: "department-32",
                name: "Кафедра математичного моделювання соціально",
                lat: 49.840168,
                lng: 24.022553
            },
            {
                id: "department-33",
                name: "Кафедра програмування",
                lat: 49.840168,
                lng: 24.022553
            },
            {
                id: "department-34",
                name: "Кафедра дискретного аналізу та інтелектуальних систем",
                lat: 49.840168,
                lng: 24.022553
            },
            {
                id: "department-35",
                name: "Кафедра інформаційних систем",
                lat: 49.840168,
                lng: 24.022553
            },
            {
                id: "department-36",
                name: "Кафедра української мови ",
                lat: 49.840168,
                lng: 24.022553
            },
            {
                id: "department-37",
                name: "Кафедра української літератури імені акад. М.Возняка",
                lat: 49.840168,
                lng: 24.022553
            },
            {
                id: "department-38",
                name: "Кафедра української фольклористики імені Ф.Колесси",
                lat: 49.840168,
                lng: 24.022553
            },
            {
                id: "department-39",
                name: "Кафедра сходознавства імені проф. Я. Дашкевича",
                lat: 49.840168,
                lng: 24.022553
            },
            {
                id: "department-40",
                name: "Кафедра російської філології",
                lat: 49.840168,
                lng: 24.022553
            },
            {
                id: "department-41",
                name: "Кафедра слов”янської філології",
                lat: 49.840168,
                lng: 24.022553
            },
            {
                id: "department-42",
                name: "Кафедра польської філології",
                lat: 49.840168,
                lng: 24.022553
            },
            {
                id: "department-43",
                name: "Кафедра прикладного українського мовознавства",
                lat: 49.840168,
                lng: 24.022553
            },
            {
                id: "department-44",
                name: "Кафедра теорії літератури та порівняльного літературознавства",
                lat: 49.840168,
                lng: 24.022553
            },
            {
                id: "department-45",
                name: "Кафедра загального мовознавства",
                lat: 49.840168,
                lng: 24.022553
            },
            {
                id: "department-46",
                name: "Кафедра філософії",
                lat: 49.840168,
                lng: 24.022553
            },
            {
                id: "department-47",
                name: "Кафедра психології ",
                lat: 49.840168,
                lng: 24.022553
            },
            {
                id: "department-48",
                name: "Кафедра політології психології ",
                lat: 49.840168,
                lng: 24.022553
            },
            {
                id: "department-49",
                name: "Кафедра теорії та історії політичної науки",
                lat: 49.840168,
                lng: 24.022553
            },
            {
                id: "department-50",
                name: "Кафедра теорії  та історії культури",
                lat: 49.840168,
                lng: 24.022553
            },
            {
                id: "department-51",
                name: "Кафедра історії філософії",
                lat: 49.840168,
                lng: 24.022553
            },
            {
                id: "department-52",
                name: "Кафедра генетики і біотехнології ",
                lat: 49.834007,
                lng: 24.031967
            },
            {
                id: "department-53",
                name: "Кафедра мікробіології",
                lat: 49.834007,
                lng: 24.031967
            },
            {
                id: "department-54",
                name: "Кафедра зоології ",
                lat: 49.834007,
                lng: 24.031967
            },
            {
                id: "department-55",
                name: "Кафедра біохімії",
                lat: 49.834007,
                lng: 24.031967
            },
            {
                id: "department-56",
                name: "Кафедра фізіології та екології рослин",
                lat: 49.834007,
                lng: 24.031967
            },
            {
                id: "department-57",
                name: "Кафедра ботаніки",
                lat: 49.834007,
                lng: 24.031967
            },
            {
                id: "department-58",
                name: "Кафедра біофізики та біоінформатики",
                lat: 49.834007,
                lng: 24.031967
            },
            {
                id: "department-59",
                name: "Кафедра екології",
                lat: 49.834007,
                lng: 24.031967
            },
            {
                id: "department-60",
                name: "Кафедра фізіології людини і тварин",
                lat: 49.834007,
                lng: 24.031967
            },
            {
                id: "department-61",
                name: "Кафедра геології корисних копалин",
                lat: 49.834007,
                lng: 24.031967
            },
            {
                id: "department-62",
                name: "Кафедра екологічної та інженерної геології і гідрогеології",
                lat: 49.834007,
                lng: 24.031967
            },
            {
                id: "department-63",
                name: "Кафедра загальної та регіональної геології",
                lat: 49.834007,
                lng: 24.031967
            },
            {
                id: "department-64",
                name: "Кафедра історичної геології та палеонтології",
                lat: 49.834007,
                lng: 24.031967
            },
            {
                id: "department-65",
                name: "Кафедра фізики землі",
                lat: 49.834007,
                lng: 24.031967
            },
            {
                id: "department-66",
                name: "Кафедра мінералогії",
                lat: 49.834007,
                lng: 24.031967
            },
            {
                id: "department-67",
                name: "Кафедра петрографії",
                lat: 49.834007,
                lng: 24.031967
            },
            {
                id: "department-68",
                name: "Кафедра економіки підприємства",
                lat: 49.842538,
                lng: 24.028204
            },
            {
                id: "department-69",
                name: "Кафедра інформаційних систем в менеджменті",
                lat: 49.842538,
                lng: 24.028204
            },
            {
                id: "department-70",
                name: "Кафедра менеджменту",
                lat: 49.842538,
                lng: 24.028204
            },
            {
                id: "department-71",
                name: "Кафедра  бухгалтерського обліку і аудиту",
                lat: 49.842538,
                lng: 24.028204
            },
            {
                id: "department-72",
                name: "Кафедра економічної кібернетики",
                lat: 49.842538,
                lng: 24.028204
            },
            {
                id: "department-73",
                name: "Кафедра фінансів, грошового обігу і кредиту",
                lat: 49.842538,
                lng: 24.028204
            },
            {
                id: "department-74",
                name: "Кафедра маркетингу",
                lat: 49.842538,
                lng: 24.028204
            },
            {
                id: "department-75",
                name: "Кафедра економічної теорії ",
                lat: 49.842538,
                lng: 24.028204
            },
            {
                id: "department-76",
                name: "Кафедра аналітичної економії та міжнародної економіки",
                lat: 49.842538,
                lng: 24.028204
            },
            {
                id: "department-77",
                name: "Кафедра статистики",
                lat: 49.842538,
                lng: 24.028204
            },
            {
                id: "department-78",
                name: "Кафедра банківського та страхового бізнесу",
                lat: 49.842538,
                lng: 24.028204
            },
            {
                id: "department-79",
                name: "Кафедра економіки України ім. М.І. Туган",
                lat: 49.842538,
                lng: 24.028204
            },
            {
                id: "department-80",
                name: "Кафедра економічної теорії",
                lat: 49.842538,
                lng: 24.028204
            },
            {
                id: "department-81",
                name: "Кафедра обліку і аудиту",
                lat: 49.842538,
                lng: 24.028204
            },
            {
                id: "department-82",
                name: "Кафедра економічної кібернетики",
                lat: 49.842538,
                lng: 24.028204
            },
            {
                id: "department-83",
                name: "Кафедра математичних методів в економіці",
                lat: 49.842538,
                lng: 24.028204
            },
            {
                id: "department-84",
                name: "Кафедра фізичної географії",
                lat: 49.837323,
                lng: 24.023253
            },
            {
                id: "department-85",
                name: "Кафедра геоморфології і палеогеографії",
                lat: 49.837323,
                lng: 24.023253
            },
            {
                id: "department-86",
                name: "Кафедра  географії України",
                lat: 49.837323,
                lng: 24.023253
            },
            {
                id: "department-87",
                name: "Кафедра грунтознавства і географії грунтів",
                lat: 49.837323,
                lng: 24.023253
            },
            {
                id: "department-88",
                name: "Кафедра економічної  та соціальної географії",
                lat: 49.837323,
                lng: 24.023253
            },
            {
                id: "department-89",
                name: "Кафедра туризму",
                lat: 49.837323,
                lng: 24.023253
            },
            {
                id: "department-90",
                name: "Кафедра раціонального використання природних ресурсів та охорони природи",
                lat: 49.837323,
                lng: 24.023253
            },
            {
                id: "department-91",
                name: "Кафедра конструктивної географії і картографії",
                lat: 49.837323,
                lng: 24.023253
            },
            {
                id: "department-92",
                name: "Кафедра адміністративного та фінансового права",
                lat: 49.839965,
                lng: 24.023376
            },
            {
                id: "department-93",
                name: "Кафедра конституційного права",
                lat: 49.839965,
                lng: 24.023376
            },
            {
                id: "department-94",
                name: "Кафедра кримінального процесу і криміналістики",
                lat: 49.839965,
                lng: 24.023376
            },
            {
                id: "department-95",
                name: "Кафедра кримінального права і кримінології",
                lat: 49.839965,
                lng: 24.023376
            },
            {
                id: "department-96",
                name: "Кафедра теорії та філософії права",
                lat: 49.839965,
                lng: 24.023376
            },
            {
                id: "department-97",
                name: "Кафедра трудового, екологічного та аграрного права",
                lat: 49.839965,
                lng: 24.023376
            },
            {
                id: "department-98",
                name: "Кафедра основ  держави та права України",
                lat: 49.839965,
                lng: 24.023376
            },
            {
                id: "department-99",
                name: "Кафедра цивільного права і процесу",
                lat: 49.839965,
                lng: 24.023376
            },
            {
                id: "department-100",
                name: "Кафедра інтелектуальної власності, інформаційного та корпоративного права ",
                lat: 49.839965,
                lng: 24.023376
            },
            {
                id: "department-101",
                name: "Кафедра історії держави, права та політики",
                lat: 49.839965,
                lng: 24.023376
            },
            {
                id: "department-102",
                name: "Кафедра міжнародного економічного аналізу і фінансів",
                lat: 49.839369,
                lng: 24.023355
            },
            {
                id: "department-103",
                name: "Кафедра міжнародних відносин і дипломатичної служби",
                lat: 49.839369,
                lng: 24.023355
            },
            {
                id: "department-104",
                name: "Кафедра міжнародного права",
                lat: 49.839369,
                lng: 24.023355
            },
            {
                id: "department-105",
                name: "Кафедра європейського права",
                lat: 49.839369,
                lng: 24.023355
            },
            {
                id: "department-106",
                name: "Кафедра міжнародних економічних відносин",
                lat: 49.839369,
                lng: 24.023355
            },
            {
                id: "department-107",
                name: "Кафедра іноземних мов факультету міжнародних відносин",
                lat: 49.839369,
                lng: 24.023355
            },
            {
                id: "department-108",
                name: "Кафедра країнознавства і міжнародного туризму",
                lat: 49.839369,
                lng: 24.023355
            },
            {
                id: "department-109",
                name: "Кафедра загальної фізики",
                lat: 49.831803,
                lng: 24.030065
            },
            {
                id: "department-110",
                name: "Кафедра  експериментальної фізики",
                lat: 49.831803,
                lng: 24.030065
            },
            {
                id: "department-111",
                name: "Кафедра теоретичної фізики",
                lat: 49.831803,
                lng: 24.030065
            },
            {
                id: "department-112",
                name: "Кафедра астрофізики",
                lat: 49.831803,
                lng: 24.030065
            },
            {
                id: "department-113",
                name: "Кафедра фізики металів",
                lat: 49.831803,
                lng: 24.030065
            },
            {
                id: "department-114",
                name: "Кафедра фізики твердого тіла",
                lat: 49.831803,
                lng: 24.030065
            },
            {
                id: "department-115",
                name: "Кафедра неорганічної хімії",
                lat: 49.831803,
                lng: 24.030065
            },
            {
                id: "department-116",
                name: "Кафедра фізичної та колоїдної хімії",
                lat: 49.831803,
                lng: 24.030065
            },
            {
                id: "department-117",
                name: "Кафедра органічної хімії",
                lat: 49.831803,
                lng: 24.030065
            },
            {
                id: "department-118",
                name: "Кафедра аналітичної хімії",
                lat: 49.831803,
                lng: 24.030065
            },
            {
                id: "department-119",
                name: "Кафедра фізичної та біомедичної електроніки",
                lat: 49.832134,
                lng: 24.027333
            },
            {
                id: "department-120",
                name: "Кафедра радіофізики  та компютерних технологій",
                lat: 49.832134,
                lng: 24.027333
            },
            {
                id: "department-121",
                name: "Кафедра фізики напівпровідників",
                lat: 49.832134,
                lng: 24.027333
            },
            {
                id: "department-122",
                name: "Кафедра радіолектронних і компютерних систем",
                lat: 49.832134,
                lng: 24.027333
            },
            {
                id: "department-123",
                name: "Кафедра оптоелектронніки та інформаційних технологій",
                lat: 49.832134,
                lng: 24.027333
            },
            {
                id: "department-124",
                name: "Кафедра електроніки",
                lat: 49.832134,
                lng: 24.027333
            },
            {
                id: "department-125",
                name: "Кафедра зарубіжної преси та інформації",
                lat: 49.830396,
                lng: 24.008704
            },
            {
                id: "department-126",
                name: "Кафедра української преси",
                lat: 49.830396,
                lng: 24.008704
            },
            {
                id: "department-127",
                name: "Кафедра теорії і практики журналістики",
                lat: 49.830396,
                lng: 24.008704
            },
            {
                id: "department-128",
                name: "Кафедра нових медій",
                lat: 49.830396,
                lng: 24.008704
            },
            {
                id: "department-129",
                name: "Кафедра радіомовлення і телебачення",
                lat: 49.830396,
                lng: 24.008704
            },
            {
                id: "department-130",
                name: "Кафедра мови засобів масової інформації",
                lat: 49.830396,
                lng: 24.008704
            },
            {
                id: "department-131",
                name: "Кафедра театрознавства та акторської майстерності",
                lat: 49.839837,
                lng: 24.03447
            },
            {
                id: "department-132",
                name: "Кафедра бібліотекознавства та бібліографії",
                lat: 49.839837,
                lng: 24.03447
            },
            {
                id: "department-133",
                name: "Кафедра режисури та хореографії",
                lat: 49.839837,
                lng: 24.03447
            },
            {
                id: "department-134",
                name: "Кафедра музичного мистецтва",
                lat: 49.839837,
                lng: 24.03447
            },
            {
                id: "department-135",
                name: "Кафедра музикознавства",
                lat: 49.839837,
                lng: 24.03447
            },
            {
                id: "department-136",
                name: "Кафедра філософії мистецтв",
                lat: 49.839837,
                lng: 24.03447
            },
            {
                id: "department-137",
                name: "Кафедра хорового співу",
                lat: 49.839837,
                lng: 24.03447
            },
            {
                id: "department-138",
                name: "Кафедра початкової та дошкільної освіти ",
                lat: 49.836838,
                lng: 24.039389
            },
            {
                id: "department-139",
                name: "Кафедра корекційної педагогіки та інклюзії",
                lat: 49.836838,
                lng: 24.039389
            },
            {
                id: "department-140",
                name: "Кафедра загальної і соціальної педагогіки",
                lat: 49.836838,
                lng: 24.039389
            }
        ]
    },
    routes: {
        title: "Маршрути",
        markerIconUrl: "img/routes.png",
        markerData: [
            {
                id: "busstop-1",
                name: "Зупинка: Залізничний вокзал",
                lat: 49.839104,
                lng: 23.996162,
                content: "<div class='delim'></div><p><strong>Трамваї: </strong> 1, 9, 10</p>"
            },
            {
                id: "busstop-2",
                name: "Зупинка: Університет ім. Івана Франка",
                lat: 49.839871,
                lng: 24.022124,
                content: "<div class='delim'></div><p><strong>Автобуси:</strong> 2А</p><p><strong>Тролейбуси: </strong> 2, 7, 9, 10, 12, 20</p>"
            },
            {
                id: "busstop-3",
                name: "Зупинка: Церква Ольги та Єлизавети",
                lat: 49.836246,
                lng: 24.004466,
                content: "<div class='delim'></div><p><strong>Трамваї: </strong>1, 9, 10, 11</p>"
            },
            {
                id: "busstop-4",
                name: "Зупинка: вулиця Пасічна",
                lat: 49.83594,
                lng: 24.068272,
                content: "<div class='delim'></div><p><strong>Трамваї: </strong>2, 7, 10</p>"
            },
            {
                id: "busstop-5",
                name: "Зупинка: вулиця Саксаганського",
                lat: 49.83408,
                lng: 24.034119,
                content: "<div class='delim'></div><p><strong>Трамваї: </strong>1, 3, 5, 11</p>"
            },
            {
                id: "busstop-6",
                name: "Зупинка: вулиця Зелена",
                lat: 49.834395,
                lng: 24.034478,
                content: "<div class='delim'></div><p><strong>Трамваї: </strong>3, 5, 9, 11</p>"
            },
            {
                id: "busstop-7",
                name: "Зупинка: пл. Івана Франка",
                lat: 49.830092,
                lng: 24.031256,
                content: "<div class='delim'></div><p><strong>Трамваї: </strong>3, 5, 9, 11</p>"
            },
            {
                id: "busstop-8",
                name: "Зупинка: пл. Івана Франка",
                lat: 49.830574,
                lng: 24.032103,
                content: "<div class='delim'></div><p><strong>Трамваї: </strong>1, 3, 5, 11</p>"
            },
            {
                id: "busstop-9",
                name: "Зупинка: вул. Варшавська",
                lat: 49.866447,
                lng: 24.016016,
                content: "<div class='delim'></div><p><strong>Тролейбуси: </strong>13</p>"
            },
            {
                id: "busstop-10",
                name: "Зупинка: вул. Варшавська",
                lat: 49.865855,
                lng: 24.016965,
                content: "<div class='delim'></div><p><strong>Тролейбуси: </strong>13</p>"
            },
            {
                id: "busstop-11",
                name: "Зупинка: площа Соборна",
                lat: 49.838156,
                lng: 24.03492,
                content: "<div class='delim'></div><p><strong>Трамваї: </strong>3, 11</p>"
            },
            {
                id: "busstop-12",
                name: "Зупинка: вул. Петра Дорошенка",
                lat: 49.839716,
                lng: 24.026141,
                content: "<div class='delim'></div><p><strong>Трамваї: </strong>2, 9, 10</p>"
            },
            {
                id: "busstop-13",
                name: "Зупинка: вул. Петра Дорошенка",
                lat: 49.840131,
                lng: 24.027176,
                content: "<div class='delim'></div><p><strong>Трамваї: </strong>1, 2, 10</p>"
            },
            {
                id: "busstop-14",
                name: "Зупинка: вул. Горбачевського",
                lat: 49.830182,
                lng: 24.007931,
                content: "<div class='delim'></div><p><strong>Трамваї: </strong>2</p>"
            },
            {
                id: "busstop-15",
                name: "Зупинка: 5та Міська лікарня",
                lat: 49.830791,
                lng: 24.008811,
                content: "<div class='delim'></div><p><strong>Трамваї: </strong>2</p>"
            },
            {
                id: "busstop-16",
                name: "Зупинка: Міжнародний аеропорт \"Львів\"",
                lat: 49.81668,
                lng: 23.956036,
                content: "<div class='delim'></div><p><strong>Тролейбуси: </strong>9</p>"
            },

        ]
    }
};